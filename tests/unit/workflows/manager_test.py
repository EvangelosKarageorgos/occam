import unittest

import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from occam.workflows.manager import WorkflowManager
from occam.workflows.database import WorkflowDatabase

from occam.workflows.datatypes.node import Node

from tests.unit.objects.mock import ObjectManagerMock

from tests.helper import multihash

from unittest.mock import patch, Mock, MagicMock


class Test__init__(unittest.TestCase):
    pass


class TestWorkflowManager:
  class TestGetInitialNodes(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      # Set up the manager we are testing
      self.workflows = WorkflowManager()

      # Maintain the object ID for the workflow
      self.workflowId = multihash()

      # Maintain ids for other objects
      self.simulatorId = multihash()
      self.simulatorRevision = multihash("sha1")
      self.dataId = multihash()
      self.dataRevision = multihash("sha1")

      # Create some objects in our mock Object Store
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "type": "workflow",
            "name": "Test Workflow",
            "file": "data.json",
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s",
                  "revision": "%s",

                  "inputs": [],
                  "outputs": []
                },
                {
                  "name": "MyData",
                  "type": "data",
                  "id": "%s",
                  "revision": "%s",

                  "inputs": [],
                  "outputs": []
                }
              ]
            }""" % (self.simulatorId, self.simulatorRevision,
                    self.dataId,      self.dataRevision)
          },
        ),

        {
          "id": self.simulatorId,
          "revision": self.simulatorRevision,
          "type": "simulator",
          "name": "MySim",

          "run": {
            "command": "usr/bin/mysim"
          }
        },

        {
          "id": self.dataId,
          "revision": self.dataRevision,
          "type": "data",
          "name": "MyData"
        },
      ])

      # Establish the workflow object
      self.workflow = self.workflows.objects.retrieve(id = self.workflowId)

      # Mock out the datastore interfact to pull out those jobs
      self.workflows.datastore = Mock(WorkflowDatabase)

    @patch('occam.person.Person')
    def test_should_return_a_list(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.resolve(self.workflow, person)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Determine that it did what it is supposed to do
      self.assertTrue(isinstance(ret, list))

    @patch('occam.person.Person')
    def test_should_return_a_tuple_with_a_Node_for_each_node(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.resolve(self.workflow, person)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Only should report the running nodes (MySim)
      self.assertTrue(len(ret) == 1)

    @patch('occam.person.Person')
    def test_should_return_an_item_for_each_runnable_object(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.resolve(self.workflow, person)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Determine that it did what it is supposed to do
      numberOfNodes = list(filter(lambda x:
                                    isinstance(x, tuple) and isinstance(x[0], Node) and isinstance(x[1], int),
                                  ret))

      # Should not have filtered anything out
      self.assertTrue(len(numberOfNodes) == len(ret))

  class TestGenerateNextTasks(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      # Set up the manager we are testing
      self.workflows = WorkflowManager()

      # Maintain the object ID for the workflow
      self.objectId = multihash()

      # Create some objects
      self.workflows.objects = ObjectManagerMock([
        {
          "id": self.objectId,
          "file": "data.json",
          "_files": {
            "data.json": "{}"
          }
        }
      ])

      # Establish some pre-existing runs/jobs

      # Mock out the datastore interfact to pull out those jobs
      self.workflows.datastore = Mock(WorkflowDatabase)

    @patch('occam.person.Person')
    def test_should_return_a_list(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      # Determine that it did what it is supposed to do
      self.assertTrue(True)
