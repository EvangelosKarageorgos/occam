import unittest

import os
import random
import sys

from occam.resources.manager import ResourceManager
from unittest.mock import patch, Mock, MagicMock, mock_open, create_autospec

sys.modules['occam.resources.plugins.svn'] = Mock()


class TestResourceManager:

  class Obj:
    def __init__(self, instanceList=True, resource_type="resource_type"):
      self.id = id
      self.uid = "uid"
      self.name = "name"
      self.identity_uri = "identity"
      self.source = "source"
      self.type = "type"
      self.subtype = "subtype"
      self.revision = "revision"
      self.resource_type = resource_type
      self.instanceList = instanceList

    def objectInfo(self):
      if self.instanceList:
        return {"install": ["cmake", "g++"]}
      else:
        return {"install": "cmake"}
    
    def retrieve(self,uid,revision,path):
      return "mockHandlerRetrieve"

    def retrieveDirectory(self,uid,revision,path,subpath):
      return {"items":"notAlist"}

    def retrieveFileStat(self,uid,revision,path,subpath):
      return {}
    
    def retrieveFile(self,uid,revision,path,subpath,start,length):
      return "status".encode('utf-8')

    def stat(self,uid,revision,path):
      return {}

  class TestHandlerFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()

    #This means that resourceType will be file after if
    def test_should_return_FileResource_object_when_resourceType_not_in_handlers(self):
      from occam.resources.plugins.file import FileResource
      handlerFor = self.resourceManager.handlerFor('fakeResourceType')
      self.assertIsInstance(handlerFor, FileResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_applicationX_XZ(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/x-xz')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_applicationX_LZMA(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/x-lzma')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_application_X_LZIP_COMPRESSED_TAR(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor(
          'application/x-lzip-compressed-tar')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_application_X_LZIP(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/x-lzip')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_application_X_TAR(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/x-tar')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_application_X_TAR(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/x-bzip2')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_TarResource_object_when_resourceType_is_application_gzip(self):
      from occam.resources.plugins.tar import TarResource
      handlerFor = self.resourceManager.handlerFor('application/gzip')
      self.assertIsInstance(handlerFor, TarResource)

    def test_should_return_plugin_GitResource_object_when_resourceType_is_git(self):
      from occam.resources.plugins.git import GitResource
      handlerFor = self.resourceManager.handlerFor('git')
      self.assertIsInstance(handlerFor, GitResource)

    def test_should_return_plugin_MercurialResource_object_when_resourceType_is_mercurial(self):
      from occam.resources.plugins.mercurial import MercurialResource
      handlerFor = self.resourceManager.handlerFor('mercurial')
      self.assertIsInstance(handlerFor, MercurialResource)

    def test_should_return_plugin_ZipResource_object_when_resourceType_is_application_zip(self):
      from occam.resources.plugins.zip import ZipResource
      handlerFor = self.resourceManager.handlerFor('application/zip')
      self.assertIsInstance(handlerFor, ZipResource)

    def test_should_return_plugin_Docker_object_when_resourceType_is_application_docker_container(self):
      from occam.resources.plugins.docker import Docker
      handlerFor = self.resourceManager.handlerFor('docker-container')
      self.assertIsInstance(handlerFor, Docker)

  class TestUidTokenFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()

    def test_should_return_bytes_given_type_name_and_resource(self):
      uidTokenFor = self.resourceManager.uidTokenFor(
          "fakeType", "fakeName", "fakeSource")
      self.assertIsInstance(uidTokenFor, bytes)

    def test_should_assert_returned_bytes_has_type_name_and_source(self):
      uidTokenFor = self.resourceManager.uidTokenFor(
          "fakeType", "fakeName", "fakeSource")
      self.assertEqual(uidTokenFor, ("type:%s\nname:%s\nsource:%s" % (
          "fakeType", "fakeName", "fakeSource")).encode('utf-8'))

  class TestUidFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.fakeObject = TestResourceManager.Obj()
      self.type = 'fakeType'
      self.name = 'fakeName'
      self.source = 'fakeSource'
      self.encodedUri = 'QmSjAU163c2dGhRGdsFz83LceaxcZm9cdhUnD4sMmM1gMB'

    def test_should_return_encoded_URI(self):
      create_autospec(self.resourceManager.uidTokenFor(
          self.type, self.name, self.source))
      uidFor = self.resourceManager.uidFor(self.type, self.name, self.source)
      self.assertEqual(uidFor, self.encodedUri)

    def test_should_assert_uidTokenFor_is_called_with_type_name_and_source(self):
      self.resourceManager.uidTokenFor = Mock(return_value="mockUidTokenFor")
      self.resourceManager.uidFor(self.type, self.name, self.source)
      self.resourceManager.uidTokenFor.assert_called_with(
          self.type, self.name, self.source)

    @patch('occam.storage.plugins.ipfs_vendor.base58.b58encode')
    @patch('occam.storage.plugins.ipfs_vendor.multihash.multihash.encode')
    def test_should_assert_multihash_encode_is_called_with_token_and_SHA256(self, multihash, b58):
      multihash.return_value = 'fakeMultihashEncode'.encode('utf-8')
      self.resourceManager.uidTokenFor = Mock(return_value="mockUidTokenFor")
      self.resourceManager.uidFor(self.type, self.name, self.source)
      b58.assert_called_with(multihash.return_value)

    @patch('occam.storage.plugins.ipfs_vendor.multihash.multihash.encode')
    def test_should_assert_multihash_encode_is_called_with_token_and_multihash_SHA256(self, multihash):
      self.resourceManager.uidTokenFor = Mock(return_value="mockUidTokenFor")
      self.resourceManager.uidFor(self.type, self.name, self.source)
      multihash.assert_called_with('mockUidTokenFor', 18)

  class TestIdTokenFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.uidFor = Mock(return_value='fakeUID')

    def test_should_return_bytes(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      self.assertIsInstance(idTokenFor, bytes)

    def test_should_assert_uidFor_is_called_with_type_name_and_source(self):
      self.resourceManager.idTokenFor("type", "name", "source", "identity")
      self.resourceManager.uidFor.assert_called_with("type", "name", "source")

    def test_should_assert_bytes_returned_contains_uid(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      uidList = idTokenFor.decode().split("\n")
      self.assertEqual(uidList[0], "uid:%s" % ("fakeUID"))

    def test_should_assert_bytes_returned_contains_identity(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      uidList = idTokenFor.decode().split("\n")
      self.assertEqual(uidList[1], "identity:%s" % ("identity"))

    def test_should_assert_bytes_returned_contains_type(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      uidList = idTokenFor.decode().split("\n")
      self.assertEqual(uidList[2], "type:%s" % ("type"))

    def test_should_assert_bytes_returned_contains_name(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      uidList = idTokenFor.decode().split("\n")
      self.assertEqual(uidList[3], "name:%s" % ("name"))

    def test_should_assert_bytes_returned_contains_source(self):
      idTokenFor = self.resourceManager.idTokenFor(
          "type", "name", "source", "identity")
      uidList = idTokenFor.decode().split("\n")
      self.assertEqual(uidList[4], "source:%s" % ("source"))

  class TestIdFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.type = 'fakeType'
      self.name = 'fakeName'
      self.source = 'fakeSource'
      self.identity = 'fakeIdentity'
      self.encodedUri = 'QmNMJJVs25rVADxsTRXKAMFFdZWzwcGXu3Adj4ehmxCPZU'

    def test_should_return_encode_URI(self):
      create_autospec(self.resourceManager.idTokenFor(
          self.type, self.name, self.source, self.identity))
      idFor = self.resourceManager.idFor(
          self.type, self.name, self.source, self.identity)
      self.assertEqual(idFor, self.encodedUri)

    def test_should_assert_idTokenFor_is_called_with_type_name_source_and_identity(self):
      self.resourceManager.idTokenFor = Mock(return_value="mockIdTokenFor")
      self.resourceManager.idFor(
          self.type, self.name, self.source, self.identity)
      self.resourceManager.idTokenFor.assert_called_with(
          self.type, self.name, self.source, self.identity)

    @patch('occam.storage.plugins.ipfs_vendor.base58.b58encode')
    @patch('occam.storage.plugins.ipfs_vendor.multihash.multihash.encode')
    def test_should_assert_multihash_encode_is_called_with_token_and_SHA256(self, multihash, b58):
      multihash.return_value = 'fakeMultihashEncode'.encode('utf-8')
      self.resourceManager.idTokenFor = Mock(return_value="mockIdTokenFor")
      self.resourceManager.idFor(
          self.type, self.name, self.source, self.identity)
      b58.assert_called_with(multihash.return_value)

    @patch('occam.storage.plugins.ipfs_vendor.multihash.multihash.encode')
    def test_should_assert_multihash_encode_is_called_with_token_and_multihash_SHA256(self, multihash):
      self.resourceManager.idTokenFor = Mock(return_value="mockIdTokenFor")
      self.resourceManager.idFor(
          self.type, self.name, self.source, self.identity)
      multihash.assert_called_with('mockIdTokenFor', 18)

  class TestInfoFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.fakeObject = TestResourceManager.Obj()

    def test_should_raise_exception_when_uid_and_id_is_None(self):
      with self.assertRaises(ValueError) as t:
        self.resourceManager.infoFor()
      self.assertTrue(
          "uid or id must be given." in str(t.exception))

    def test_should_return_dictionary_when_retrieveResource_returns_data(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertIsInstance(infoFor, dict)

    def test_should_assert_retrieveResource_is_called_with_uid_and_id(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      self.resourceManager.infoFor("id", "uid")
      self.resourceManager.retrieveResource.assert_called_with(
          uid="uid", id="id")

    def test_should_return_None_when_retrieveResource_returns_None(self):
      self.resourceManager.retrieveResource = Mock(return_value=None)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor, None)

    def test_should_assert_dictionary_id_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("id"), self.fakeObject.id)

    def test_should_assert_dictionary_uid_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("uid"), self.fakeObject.uid)

    def test_should_assert_dictionary_identity_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("identity"), self.fakeObject.identity_uri)

    def test_should_assert_dictionary_name_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("name"), self.fakeObject.name)

    def test_should_assert_dictionary_source_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("source"), self.fakeObject.source)

    def test_should_assert_dictionary_type_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("type"), "resource")

    def test_should_assert_dictionary_subtype_is_the_same_as_retrieveResource_returned_object(self):
      self.resourceManager.retrieveResource = Mock(
          return_value=self.fakeObject)
      infoFor = self.resourceManager.infoFor("id", "uid")
      self.assertEqual(infoFor.get("subtype"), [self.fakeObject.resource_type])

  class TestPathFor(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.storage.resourcePathFor = Mock(
          return_value='fakeResourcePathFor')
      self.resourceManager.datastore = Mock()
      self.resourceManager.datastore.retrieveResource = Mock(return_value=None)
      self.fakeObject = TestResourceManager.Obj()

    def test_should_return_None_when_handler_is_None_and_datastore_retrieveResource_returns_None(self):
      self.resourceManager.datastore.retrieveResource = Mock(return_value=None)
      pathFor = self.resourceManager.pathFor("uid", "revision")
      self.assertEqual(pathFor, None)

    def test_should_assert_datastore_retrieveResource_is_called_with_uid(self):
      self.resourceManager.datastore.retrieveResource = Mock(return_value=None)
      self.resourceManager.pathFor("uid", "revision")
      self.resourceManager.datastore.retrieveResource.assert_called_with(
          uid="uid")

    def test_should_return_None_when_handler_is_None_and_datastore_retrieveResource_returns_object_with_resource_type_None(self):
      self.fakeObject = TestResourceManager.Obj(resource_type=None)
      self.resourceManager.datastore.retrieveResource = Mock(
          return_value=self.fakeObject)
      pathFor = self.resourceManager.pathFor("uid", "revision")
      self.assertEqual(pathFor, None)

    @patch('occam.resources.plugins.file.FileResource')
    def test_should_assert_handlerFor_is_called_with_resourceType(self, FileResource):
      fileResource = FileResource()
      self.resourceManager.handlerFor = Mock(return_value=fileResource)
      self.resourceManager.datastore.retrieveResource = Mock(
          return_value=self.fakeObject)
      self.resourceManager.pathFor("uid", "revision")
      self.resourceManager.handlerFor.assert_called_with(
          self.fakeObject.resource_type)

    @patch('occam.resources.plugins.file.FileResource')
    def test_should_return_string_containing_path_for_revision(self, FileResource):
      fileResource = FileResource()
      fileResource.pathFor.return_value = "mockPathFor/revision"
      pathFor = self.resourceManager.pathFor(
          "uid", "revision", handler=fileResource)
      self.assertIsInstance(pathFor, str)

    @patch('occam.resources.plugins.file.FileResource')
    def test_should_assert_storage_resourcePathFor_is_called_with_uid_and_revision(self, FileResource):
      fileResource = FileResource()
      fileResource.pathFor.return_value = "mockPathFor/revision"
      self.resourceManager.pathFor("uid", "revision", handler=fileResource)
      self.resourceManager.storage.resourcePathFor.assert_called_with(
          "uid", "revision")

    @patch('occam.resources.plugins.file.FileResource')
    def test_should_assert_handler_pathFor_is_called_with_uid_storagePath_and_revision(self, FileResource):
      fileResource = FileResource()
      fileResource.pathFor.return_value = "mockPathFor/revision"
      self.resourceManager.handlerFor = Mock(return_value=fileResource)
      self.resourceManager.pathFor("uid", "revision", handler=fileResource)
      self.resourceManager.handlerFor.return_value.pathFor.assert_called_with(
          "uid", "fakeResourcePathFor", "revision")

  class TestInstall(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.storage.resourcePathFor = Mock(
          return_value="mockStoragePath")

    def test_should_return_when_resourceInfo_None(self):
      self.assertEqual(self.resourceManager.install(None, "path"), None)

    def test_should_return_False_when_string_double_dots_in_resourceInfo_to(self):
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "../to/somewhere"}
      install = self.resourceManager.install(resourceInfo, "path")
      self.assertEqual(install, False)

    def test_should_return_False_when_revision_not_None_uid_is_None_and_double_dots_not_in_resourceInfo_and_resourcePath_is_None(self):
      resourceInfo = {"id": "id", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      install = self.resourceManager.install(resourceInfo, "path")
      self.assertEqual(install, False)

    def test_should_return_False_when_revision_and_uid_is_None_and_double_dots_not_in_resourceInfo_and_resourcePath_is_None(self):
      resourceInfo = {"id": "id", "subtype": "fakeSubType",
                      "to": "path/to/somewhere"}
      install = self.resourceManager.install(resourceInfo, "path")
      self.assertEqual(install, False)

    def test_should_return_mockedString_when_revision_and_uid_not_None_and_handler_exists_returns_true_with_uid_storagePath_revision_and_handler_install_return_data(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      install = self.resourceManager.install(resourceInfo, "path")
      self.assertEqual(install, "MockReturnHandlerInstall")

    def test_should_return_mockedString_when_revision_and_uid_not_None_and_handler_exists_returns_true_with_uid_storagePath_revision_and_handler_install_return_False(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = False
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      install = self.resourceManager.install(resourceInfo, "path")
      self.assertEqual(install, False)

    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      self.resourceManager.handlerFor.assert_called_with('fakeSubType')

    def test_should_assert_storage_resourcePathFor_is_called_with_uid_and_revision(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      self.resourceManager.storage.resourcePathFor.assert_called_with(
          "uid", "revision")

    def test_should_assert_handler_exists_is_called_with_uid_storagePath_and_revision(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      self.resourceManager.handlerFor.return_value.exists.assert_called_with(
          "uid", "mockStoragePath", "revision")

    def test_should_assert_handler_pathFor_is_called_with_uid_storagePath_and_revision(self):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      self.resourceManager.handlerFor.return_value.pathFor.assert_called_with(
          "uid", "mockStoragePath", "revision")

    @patch('os.path.realpath', return_value="mockRealPath")
    def test_should_assert_handler_install_is_called_with_uid_revision_resourcePath_resourceInfo_destination_equals_to_realPath(self, path):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.pathFor.return_value = "mockPathFor"
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      path.assert_called_with("path")
      self.resourceManager.handlerFor.return_value.install.assert_called_with(
          "uid", "revision", "mockPathFor", resourceInfo, destination=path.return_value)

    @patch('os.path.realpath', return_value="mockRealPath")
    def test_should_assert_handler_actions_is_called_with_uid_revision_resourcePath_resourceInfo_destination_equals_to_realPath(self, path):
      self.resourceManager.handlerFor.return_value.exists.return_value = True
      self.resourceManager.handlerFor.return_value.pathFor.return_value = "mockPathFor"
      self.resourceManager.handlerFor.return_value.install.return_value = "MockReturnHandlerInstall"
      resourceInfo = {"id": "id", "uid": "uid", "revision": "revision",
                      "subtype": "fakeSubType", "to": "path/to/somewhere"}
      self.resourceManager.install(resourceInfo, "path")
      path.assert_called_with("path")
      self.resourceManager.handlerFor.return_value.install.assert_called_with(
          "uid", "revision", "mockPathFor", resourceInfo, destination=path.return_value)

  class TestInstallAll(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.install = Mock()

    def test_should_return_empty_list_when_install_not_in_objectInfo(self):
      objectInfo = {}
      installAll = self.resourceManager.installAll(objectInfo, "path")
      self.assertEqual(installAll, [])

    def test_should_return_list_with_install_resources_and_string_install_in_resource(self):
      objectInfo = {"install": [{"type": "resource", "resource": "C++", "install": [
          {"dependency": True, "type": "resource", "resource": "iostream"}]}]}
      installAll = self.resourceManager.installAll(objectInfo, "path")
      self.assertIsInstance(installAll, list)
      self.assertEqual(installAll, objectInfo.get('install'))

    def test_should_return_list_with_install_resources_and_type_not_equal_resource(self):
      objectInfo = {"install": [{"type": "compiler", "resource": "C++", "install": [
          {"dependency": True, "type": "resource", "resource": "iostream"}]}]}
      installAll = self.resourceManager.installAll(objectInfo, "path")
      self.assertIsInstance(installAll, list)
      self.assertEqual(installAll, objectInfo.get('install'))

    def test_should_assert_install_is_called_with_resource_and_path(self):
      objectInfo = {"install": [{"type": "resource", "resource": "C++"}]}
      self.resourceManager.installAll(objectInfo, "path")
      self.resourceManager.install.assert_called_with(
          objectInfo.get('install')[0], "path")

  class TestCloneAll(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.clone = Mock(return_value = "mockClone")

    def test_should_return_list_when_resources_is_instance_list(self):
      fakeObject = TestResourceManager.Obj()
      cloneAll = self.resourceManager.cloneAll(fakeObject)
      self.assertIsInstance(cloneAll,list)

    def test_should_return_list_when_resources_is_not_instance_list(self):
      fakeObject = TestResourceManager.Obj(instanceList = False)
      cloneAll = self.resourceManager.cloneAll(fakeObject)
      self.assertIsInstance(cloneAll,list)
    
    def test_should_assert_returned_list_has_new_info(self):
      fakeObject = TestResourceManager.Obj(instanceList = False)
      cloneAll = self.resourceManager.cloneAll(fakeObject)
      self.assertEqual(cloneAll,["mockClone"])

    def test_should_assert_clone_is_called_with_resourceInfo(self):
      fakeObject = TestResourceManager.Obj(instanceList = False)
      self.resourceManager.cloneAll(fakeObject)
      self.resourceManager.clone.assert_called_with("cmake")

  class TestClone(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceInfo = {"type": "type", "id": "id",
                           "revision": "revision", "source": "source", "name": "name", "to": "to"}

    def test_should_return_original_resourceInfo_when_object_is_not_clonable(self):
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.clonable.return_value = False
      clone = self.resourceManager.clone(self.resourceInfo)
      self.assertEqual(clone,self.resourceInfo)

    def test_should_overwrite_identifying_tags_in_the_new_resource_when_object_is_clonable(self):
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.clonable.return_value = True
      self.resourceManager.handlerFor.return_value.clone.return_value = {"id":"newId","revision":"newRevision"}
      clone = self.resourceManager.clone(self.resourceInfo)
      self.assertEqual(clone.get("id"),"newId")
      self.assertEqual(clone.get("revision"),"newRevision")

    def test_should_assert_handlerFor_is_called_with_installType(self):
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.clonable.return_value = True
      self.resourceManager.handlerFor.return_value.clone.return_value = {"id":"newId","revision":"newRevision"}
      self.resourceManager.clone(self.resourceInfo)
      self.resourceManager.handlerFor.assert_called_with(self.resourceInfo.get("type"))

    def test_should_assert_handlerFor_clone_is_called_with_uid_revision_name_source_to(self):
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.clonable.return_value = True
      self.resourceManager.clone(self.resourceInfo)
      self.resourceManager.handlerFor.return_value.clone.assert_called_with(self.resourceInfo.get("id"),self.resourceInfo.get("revision"),self.resourceInfo.get("name"),self.resourceInfo.get("source"),self.resourceInfo.get("to"))
      
  class TestRetrieveFile(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.retrieve.return_value = "mockHandlerRetrieve"
      self.resourceManager.pathFor = Mock(return_value = "mockPathFor")

    def test_should_return_path_to_resource(self):
      retrieveFile = self.resourceManager.retrieveFile("id","uid","revision","resourceType")
      self.assertEqual(retrieveFile,"mockHandlerRetrieve")

    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.retrieveFile("id","uid","revision","resourceType")
      self.resourceManager.handlerFor.assert_called_with("resourceType")

    def test_should_assert_pathFor_is_called_with_resourceType(self):
      fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock(return_value = fakeObject)
      self.resourceManager.retrieveFile("id","uid","revision","resourceType")
      self.resourceManager.pathFor.assert_called_with("uid","revision",handler = fakeObject)

    def test_should_assert_handlerFor_retrieve_is_called_with_resourceType(self):
      self.resourceManager.retrieveFile("id","uid","revision","resourceType")
      self.resourceManager.handlerFor.return_value.retrieve.assert_called_with("uid","revision","mockPathFor")

  class TestRetrieveFileStat(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.handlerFor.return_value.stat.return_value = {}
      self.resourceManager.pathFor = Mock(return_value = "mockPathFor")

    def test_should_return_dict(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertIsInstance(retrieveFileStat, dict)

    def test_should_assert_dictionary_from_has_type_equal_to_string_resource(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("type"),"resource")

    def test_should_assert_dictionary_from_has_subtype_equal_to_parameter_resourceType(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("subtype"), ["resourceType"])

    def test_should_assert_dictionary_from_has_id_equal_to_parameter_id(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("id"),"id")

    def test_should_assert_dictionary_from_has_uid_equal_to_parameter_uid(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("uid"),"uid")

    def test_should_assert_dictionary_from_has_revision_equal_to_parameter_revision(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("revision"),"revision")

    def test_should_assert_dictionary_from_has_path_equal_to_parameter_path(self):
      retrieveFileStat = self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.assertEqual(retrieveFileStat["from"].get("path"),"mockPathFor")

    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.resourceManager.handlerFor.assert_called_with("resourceType")

    def test_should_assert_pathFor_is_called_with_uid_revision_and_handler(self):
      fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock(return_value = fakeObject)
      self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.resourceManager.pathFor.assert_called_with("uid","revision",handler = fakeObject)

    def test_should_assert_handler_stat_is_called_with_uid_revision_path(self):
      self.resourceManager.retrieveFileStat("id","uid","revision","resourceType")
      self.resourceManager.handlerFor.return_value.stat.assert_called_with("uid","revision","mockPathFor")

  class TestRetrieveDirectoryFrom(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.pathFor = Mock()
      self.resourceManager.handlerFor.return_value.retrieveDirectory.return_value = {
        "items": [
          {"name": "foo"},
          {"name": "foo"},
          {"name": "foo"},
          {"name": "foo"}
        ]
      }
  
    def test_should_return_dict_when_data_items_is_isintance_list(self):
     retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
     self.assertIsInstance(retrieveFileStat,dict)

    def test_should_turn_data_get_items_into_empty_list_and_return_dictionary(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertIsInstance(retrieveFileStat.get("items"),list)

    def test_should_assert_returned_dictionary_at_from_has_type_equals_to_resource(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("type"),"resource")

    def test_should_assert_returned_dictionary_at_from_has_subtype_equals_to_resourceType(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("subtype"),["resourceType"])

    def test_should_assert_returned_dictionary_at_from_has_id_equals_to_id(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("id"),"id")

    def test_should_assert_returned_dictionary_at_from_has_uid_equals_to_uid(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("uid"),"uid")
    
    def test_should_assert_returned_dictionary_at_from_has_revision_equals_to_revision(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("revision"),"revision")

    def test_should_assert_returned_dictionary_at_from_has_path_equals_to_subpath(self):
      retrieveFileStat = self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.assertEqual(retrieveFileStat['from'].get("path"),"subpath")

    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.resourceManager.handlerFor.assert_called_with("resourceType")

    def test_should_assert_pathFor_is_called_with_uid_revision_and_handler(self):
      fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock(return_value = fakeObject)
      self.resourceManager.retrieveDirectoryFrom("id","uid","revision","resourceType","subpath")
      self.resourceManager.pathFor.assert_called_with("uid","revision",handler = fakeObject)

  class TestRetrieveSource(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.datastore = Mock()

    @patch('occam.resources.records.resource.ResourceRecord')
    def test_should_return_ResourceRecord_object_when_all_parameters_are_None(self,ResourceRecord):
      resourceRecord = ResourceRecord()
      self.resourceManager.datastore.retrieveResource.return_value = resourceRecord
      retrieveResource = self.resourceManager.retrieveResource()
      self.assertIs(retrieveResource,resourceRecord)

    @patch('occam.resources.records.resource.ResourceRecord')
    def test_should_assert_datastore_retrieveResource_is_called_with_all_parameters_None(self,ResourceRecord):
      resourceRecord = ResourceRecord()
      self.resourceManager.datastore.retrieveResource.return_value = resourceRecord
      self.resourceManager.retrieveResource()
      self.resourceManager.datastore.retrieveResource.assert_called_with(resourceType=None, id=None, uid=None, revision=None, source=None)

    @patch('occam.resources.records.resource.ResourceRecord')
    def test_should_return_ResourceRecord_object_when_all_parameters_are_not_None(self,ResourceRecord):
      resourceRecord = ResourceRecord()
      self.resourceManager.datastore.retrieveResource.return_value = resourceRecord
      retrieveResource = self.resourceManager.retrieveResource(resourceType="resourceType", id="id", uid="uid", revision="revision", source="source")
      self.assertIs(retrieveResource,resourceRecord)

    @patch('occam.resources.records.resource.ResourceRecord')
    def test_should_assert_datastore_retrieveResource_is_called_with_all_parameters_not_None(self,ResourceRecord):
      resourceRecord = ResourceRecord()
      self.resourceManager.datastore.retrieveResource.return_value = resourceRecord
      self.resourceManager.retrieveResource(resourceType="resourceType", id="id", uid="uid", revision="revision", source="source")
      self.resourceManager.datastore.retrieveResource.assert_called_with(resourceType="resourceType", id="id", uid="uid", revision="revision", source="source")

  class TestRetrieveFileStatFrom(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.pathFor = Mock(return_value="mockPathFor")

    def test_should_return_dictionary_status_from_file(self):
      retrieveFileStatFrom = self.resourceManager.retrieveFileStatFrom(1,2,"revision","resourceType","subpath")
      self.assertIsInstance(retrieveFileStatFrom, dict)
      
    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.retrieveFileStatFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.handlerFor.assert_called_with("resourceType")

    def test_should_assert_pathFor_is_called_with_uid_revision_and_handler(self):
      fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock(return_value = fakeObject)
      self.resourceManager.retrieveFileStatFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.pathFor.assert_called_with(2,"revision",handler = fakeObject)

    def test_should_assert_handler_stat_is_called_with_uid_revision_path(self):
      self.resourceManager.retrieveFileStatFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.handlerFor.return_value.retrieveFileStat.assert_called_with(2,"revision","mockPathFor","subpath")

  class TestRetrieveFileFrom(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      self.resourceManager = ResourceManager()
      self.resourceManager.handlerFor = Mock()
      self.resourceManager.pathFor = Mock(return_value="mockPathFor")

    def test_should_return_bytes(self):
      retrieveFileFrom = self.resourceManager.retrieveFileFrom(1,2,"revision","resourceType","subpath")
      self.assertIsInstance(retrieveFileFrom,bytes)

    def test_should_assert_handlerFor_is_called_with_resourceType(self):
      self.resourceManager.retrieveFileFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.handlerFor.assert_called_with("resourceType")

    def test_should_assert_pathFor_is_called_with_uid_revision_and_handler(self):
      fakeObject = TestResourceManager.Obj()
      self.resourceManager.handlerFor = Mock(return_value = fakeObject)
      self.resourceManager.retrieveFileFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.pathFor.assert_called_with(2,"revision",handler = fakeObject)

    def test_should_assert_handler_stat_is_called_with_uid_revision_path(self):
      self.resourceManager.retrieveFileFrom(1,2,"revision","resourceType","subpath")
      self.resourceManager.handlerFor.return_value.retrieveFile.assert_called_with(2,"revision","mockPathFor","subpath",length=None,start=0)
    
