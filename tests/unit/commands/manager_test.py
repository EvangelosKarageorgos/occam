import unittest

import os, random
random.seed(os.environ["PYTHONHASHSEED"])

import re

from occam.commands.manager import CommandManager

class GenerateRandomObjectIdentifiers:

  @staticmethod
  def generate_random_uuid():
    import uuid
    uid = str(uuid.uuid4())
    return uid

  @staticmethod
  def generate_random_version():
    version = "" + str(random.randint(0,10))
    for i in range(1,random.randint(2,4)):
      version += "." + str(random.randint(0,100))
    return version

  @staticmethod
  def generate_random_revision():
    import hashlib
    numbers=list("0123456789ABCDEF")
    random.shuffle(numbers)
    revision = hashlib.sha1("".join(numbers).encode("ascii")).hexdigest()
    return revision

  @staticmethod
  def generate_random_link():
    link = str(random.randint(0,10000000))
    return link

  @staticmethod
  def generate_random_index():
    index_max_depth = random.randint(1,10)
    index = ""
    for i in range(0,index_max_depth):
      index += "[" + str(random.randint(0,100)) + "]"
    return index

  @staticmethod
  def generate_random_path():
    import os
    import string
    path = "/"+''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits+os.path.sep+".") for _ in range(random.randint(10,250)) )
    return path

  @staticmethod
  def generate_random_object_identifier(config=None):
    if config is None:
      config = {}
      for item in ["version", "revision", "link", "index", "path"]:
        config[item] = config.get(item, False)

    # The user can specify the uuid
    uuid = config.get("uuid", None)
    if( uuid is None ):
      uuid = GenerateRandomObjectIdentifiers.generate_random_uuid()
    obj_id = uuid

    if( config.get("version") ):
      version = GenerateRandomObjectIdentifiers.generate_random_version()
      obj_id += ":" + version
    else:
      version = None

    if( config.get("revision") ):
      revision = GenerateRandomObjectIdentifiers.generate_random_revision()
      obj_id += "@" + revision
    else:
      revision = None

    if( config.get("link") ):
      link = GenerateRandomObjectIdentifiers.generate_random_link()
      obj_id += "#" + link
    else:
      link = None

    if( config.get("index") ):
      index = GenerateRandomObjectIdentifiers.generate_random_index()
      obj_id += index
    else:
      index = None

    if( config.get("path") ):
      path = GenerateRandomObjectIdentifiers.generate_random_path()
      obj_id += path
    else:
      path = None

    return obj_id, {
      "uuid"     : uuid,
      "version"  : version,
      "revision" : revision,
      "link"     : link,
      "index"    : index,
      "path"     : path
    }

  @staticmethod
  def parse_identifier_into_elements(regex, identifier):
    parser = re.compile(regex)
    result = parser.match(identifier)

    if result is None:
      return None

    return {
      "uuid"     : result.group("uuid"),
      "version"  : result.group("version"),
      "revision" : result.group("revision"),
      "link"     : result.group("link"),
      "index"    : result.group("index"),
      "path"     : result.group("path")
    }
class TestManager:

  invalid_object_identifiers = [
  ]

  class TestGetObjectIdentifierRegex(unittest.TestCase):

    regex = CommandManager.getObjectIdentifierRegex()


    def test_should_identify_object_identifier_uuid(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier()
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_repository_uuid(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({"uuid": "+"})
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_localdir_uuid(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({"uuid": "."})
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_version(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "version": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_link(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "link": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_index(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "index": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_link_index(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "link": True,
        "index": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_index_path(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "index": True,
        "path": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_link_path(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "link": True,
        "path": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_uuid_revision_link_index_path(self):
      obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
        "revision": True,
        "link": True,
        "index": True,
        "path": True
      })
      result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
      self.assertIsNotNone(result)
      if( result is not None ):
        for k,v in indentifier.items():
          self.assertEqual(result.get(k), v)

    def test_should_identify_object_identifier_random_elements(self):
      # Do some random combinations to possibly find unexpected results
      for i in range(0,1000):
        obj_id, indentifier = GenerateRandomObjectIdentifiers.generate_random_object_identifier({
          "version"  : True,
          "revision" : True,
          "link"     : True,
          "index"    : True,
          "path"     : True
        })
        result = GenerateRandomObjectIdentifiers.parse_identifier_into_elements(self.regex, obj_id)
        try:
          self.assertIsNotNone(result)
          if( result is not None ):
            for k,v in indentifier.items():
              self.assertEqual(result.get(k), v)
        except Exception as e:
          print("Object identifier: " + obj_id)
          print("Input: ")
          for k,v in indentifier.items():
            print("{0}: {1}".format(k,v))
          print("Output: ")
          for k,v in result.items():
            print("{0}: {1}".format(k,v))
          raise e
