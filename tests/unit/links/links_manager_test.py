import unittest

from occam.links.manager import LinkManager
from occam.links.records.link import LinkRecord
from occam.links.records.local_link import LocalLinkRecord
from occam.objects.records.object import ObjectRecord
from occam.objects.records.person import PersonRecord
from unittest.mock import patch, Mock, MagicMock, mock_open, create_autospec, DEFAULT

from tests.helper import uuid

class TestLinkManager:
  pass
