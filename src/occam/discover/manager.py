# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import ipaddress
from types import SimpleNamespace

from occam.object   import Object
from occam.config   import Config
from occam.log      import loggable
from occam.semver   import Semver
from occam.datetime import Datetime

from occam.manager import manager, uses

from occam.storage.manager         import StorageManager
from occam.nodes.manager           import NodeManager
from occam.keys.write_manager      import KeyWriteManager
from occam.network.manager         import NetworkManager
from occam.objects.write_manager   import ObjectWriteManager
from occam.resources.write_manager import ResourceWriteManager
from occam.permissions.manager     import PermissionManager
from occam.versions.write_manager  import VersionWriteManager
from occam.builds.write_manager    import BuildWriteManager

@loggable
@manager("discover")
@uses(StorageManager)
@uses(NodeManager)
@uses(NetworkManager)
@uses(KeyWriteManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
@uses(ResourceWriteManager)
@uses(VersionWriteManager)
@uses(BuildWriteManager)
class DiscoverManager:
  """ This manages mechanisms for finding objects in the world at large. This
      gives the discovery capability to any module that needs it.

      Discovery is sometimes passed into routines that might require it. For
      instance, routines that might lookup subobjects that are linked might
      want a discovery agent to use to do that lookup. You can provide this
      manager as that agent.

      It is possible that down the line we will allow alternative agents or
      extensions that will enable different modes of discovery through
      services or various archive or publication venues that are outside of
      the federation.
  """

  handlers = {}
  handlerImpl = []
  instantiated = {}

  def __init__(self):
    import occam.discover.plugins.ipfs

  @staticmethod
  def register(networkName, handlerClass):
    """ Adds a new discover backend type.

    Arguments:
      handlerClass (object): The handler that implements the discovery interface.
    """

    DiscoverManager.handlers[networkName] = {
      'class': handlerClass
    }

  def handlerFor(self, networkName):
    """ Returns an instance of a handler for the given name.
    """

    if not networkName in DiscoverManager.handlers:
      DiscoverManager.Log.error("discovery network backend %s not known" % (networkName))
      return None

    # Instantiate a storage backend if we haven't seen it before
    if not networkName in DiscoverManager.instantiated:
      # Pull the configuration (from the 'stores' subpath and keyed by the name)
      subConfig = self.configurationFor(networkName)

      # Create a driver instance
      instance = DiscoverManager.handlers[networkName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      # TODO: cache this in the stores configuration? or detection file?
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      DiscoverManager.instantiated[networkName] = instance

    if DiscoverManager.instantiated[networkName] is None:
      # The driver could not be initialized
      return None

    return DiscoverManager.instantiated[networkName]

  def defaultBackend(self):
    """ Returns the default discovery backend.
    """

    return self.configuration.get("default", "ipfs")

  def configurationFor(self, networkName):
    """ Returns the configuration for the given discovery network.
    
    This configuration is found within the occam configuration (config.yml)
    under the "discover" section under the given plugin name.
    """

    config = self.configuration
    subConfig = config.get(networkName, {})

    return subConfig

  def daemon(self):
    """ Starts a discovery daemon.
    """

    # Start an IPFS daemon

  def announce(self, id, token, backend=None):
    """ Announces a particular object id on the federation.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    handler.announce(id, token)

  def announceIdentity(self, id, publicKey, backend=None):
    """ Announces the public key with the identifier.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    handler.announceIdentity(id, publicKey)

  def announceViewer(self, type, subtype, backend=None):
    """ Announces that there is a viewer for the given type and subtype.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    id    = self.viewerIdFor(type, subtype, backend=backend)
    token = self.viewerTokenFor(type, subtype, backend=backend)

    handler.announce(id, token)

  def announceEditor(self, type, subtype, backend=None):
    """ Announces that there is a editor for the given type and subtype.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return

    id    = self.editorIdFor(type, subtype, backend=backend)
    token = self.editorTokenFor(type, subtype, backend=backend)
    handler.announce(id, token)

  def retrieveToken(self, id, backend=None):
    """
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    ret = handler.retrieveToken(id)

    return ret

  def nodesWithId(self, id, backend=None, limit=10):
    """
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return []

    nodes = handler.search(id)

    ret = []

    for address in nodes:
      if ipaddress.ip_address(address).is_private:
        continue
      hostURL = "https://%s:9292" % (address)

      node = self.nodes.search(hostURL)
      if node is None:
        node = self.nodes.discover(hostURL, untrusted=True, quiet=True)

      ret.append(node)

      if len(ret) == limit:
        break

    return ret

  def retrieveJSON(self, option, person = None, backend=None):
    """
    """

    data = self.retrieveFile(option, person, backend)
    if data:
      import json
      import codecs
      try:
        reader = codecs.getreader('utf-8')
        data = json.load(reader(data))
      except:
        pass

    return data

  def retrieveFile(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.retrieveFileFrom(nodes[0], option, person=person)

  def retrieveDirectory(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.retrieveDirectoryFrom(nodes[0], option, person=person)

  def editorIdFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorIdFor(type, subtype)

  def editorTokenFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorTokenFor(type, subtype)

  def editorsFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers editors for the given type and optional subtype.
    """

    id = self.editorIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.editorsFor(nodes[0], type, subtype, person=person)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for editor in data:
      # Create a editor/object record with the data
      editor['identity_uri'] = editor.get('identity')
      ret.append(ObjectRecord(editor))

    return ret

  def viewerIdFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerIdFor(type, subtype)

  def viewerTokenFor(self, type, subtype, backend):
    """
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerTokenFor(type, subtype)

  def viewersFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers viewers for the given type and optional subtype.
    """

    id = self.viewerIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.viewersFor(nodes[0], type, subtype, person=person)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for viewer in data:
      # Create a viewer/object record with the data
      viewer['identity_uri'] = viewer.get('identity')
      ret.append(ObjectRecord(viewer))

    return ret

  def taskFor(self):
    """ Discovers any existing tasks for the given properties.
    """

  def providersFor(self, type, subtype = None, person=None, backend=None):
    """ Discovers providers for the given environment and architecture.
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.viewersFor(nodes[0], type, subtype, person=person)

  def status(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.statusFrom(nodes[0], option, person=person)

  def history(self, option, person = None, backend=None):
    """
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.historyFrom(nodes[0], option, person=person)

  def resolve(self, option, person = None):
    """ Returns an Object based on a CLI object argument.

    If it is given a URL, it attempts to discover the object through the normal
    means. The scheme of the url will depict the storage layer to query.

    If it is not a URL, but an Occam uuid and revision, it will search for the
    object within the federation and discover it through the normal channels.
    """

    if option is None:
      return None

    if self.network.isURL(option.id):
      parts = self.network.parseURL(option.id)
      objInfo = self.storage.retrieve(parts.scheme, parts.netloc)

      ret = None
    else:
      # TODO: check the normal local repository and then react only if the
      #       object is not found.
      ret = self.objects.resolve(option, person = person)
      if ret is None:
        ret = self.discover(option.id, revision = option.revision)

    return ret

  def retrieveIdentity(self, uri, person=None, backend=None, nodes = None):
    """ Discovers the given identity.
    """

    import base64, datetime

    # Retrieve public key (IPFS, etc)
    publicKey = self.retrieveToken(uri, backend=backend)

    identityInfo = None

    if not publicKey:
      # Discover identity the hard way... if necessary
      # TODO: only do this if there are no federated backends
      nodes = nodes or self.nodes.findIdentity(uri)

      if len(nodes) > 0:
        try:
          identityInfo = self.nodes.identityFrom(nodes[0], uri)
          keyInfo = identityInfo["publicKey"]
          if keyInfo.get('format') == "PEM" and keyInfo.get('encoding') == "base64":
            publicKey = base64.b64decode(keyInfo.get('data')).decode('utf-8')
        except:
          pass

    if not publicKey:
      DiscoverManager.Log.write("cannot find identity")
      return None

    # Discover this identity
    identity = self.keys.write.discover(uri, publicKey)
    if not identity:
      # The KeyManager rejected this identity
      DiscoverManager.Log.error("key rejected")
      return None

    # Retrieve everything else
    nodes = nodes or self.nodesWithId(uri, backend=backend)

    if not nodes:
      return None

    identityInfo = identityInfo or self.nodes.identityFrom(nodes[0], uri)

    # Look at known signing keys (and their signatures)
    # Verify each of them and commit them
    for verifyKey in identityInfo.get('verifyingKeys', []):
      # {
      #   'key': { 'data': '', 'encoding': 'base64' },
      #   'signature': { 'data': '', 'encoding': 'base64' }
      # }
      try:
        keyInfo = verifyKey.get('key')
        id = keyInfo.get('id')
        DiscoverManager.Log.write(f"discovering verification key {id}")
        published = Datetime.from8601(keyInfo.get('published'))
        key = None

        if keyInfo.get('format') == "PEM" and keyInfo.get('encoding') == "base64":
          key = base64.b64decode(keyInfo.get('data')).decode('utf-8')

        signatureInfo = verifyKey.get('signature')
        signature = None

        if signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64":
          signature = base64.b64decode(signatureInfo.get('data'))
      except Exception as e:
        # Invalid key if there is any strange encoding error
        continue

      DiscoverManager.Log.write(f"accepted verification key {id}")
      verifyKey = self.keys.write.discoverKey(uri, id, key, signature, published, None, publicKey)

    return identityInfo

  def pullResource(self, node, objStat, uuid, revision=None, person = None):
    """ Pulls a resource object from the given node.
    """

    # Gather identity URI
    identity = objStat.get('identity')

    # Get the resource type (defaults to 'file')
    resourceType = objStat.get('subtype', ["file"])
    if not isinstance(resourceType, list):
      resourceType = [resourceType]
    resourceType = resourceType[0]

    DiscoverManager.Log.write(f"pulling {resourceType} resource")

    url = self.nodes.urlForNode(node, path = self.resources.urlFor(uuid, revision = revision, resourceType = resourceType))
    self.resources.write.pull(objStat, identity, overrideSource = url)

  def pullObject(self, node, objStat, uuid, revision = None, version = None, withResources = True, withDependencies = True, withBuild = True, withBuildDependencies = False, person = None):
    """ Pulls an object from the given node.
    """

    import base64, datetime

    # Revise what we are pulling to get the owner instead
    ownerId = objStat.get('owner', {}).get('id', uuid)

    # Gather identity URI
    identity = objStat.get('identity')

    # Pull the identity
    self.retrieveIdentity(identity, nodes = [node])

    # If we need a version, we have to pull version information down to acquire the right revision
    versions = self.nodes.pullVersionsFrom(node, ownerId)
    versions = versions.get('versions', versions.get('tags', []))
    resolvedVersion = version
    if version and not revision:
      versionList = [versionInfo.get('version') for versionInfo in versions]
      resolvedVersion = Semver.resolveVersion(version, versionList)
      
      for versionInfo in versions:
        if versionInfo.get('version') == resolvedVersion:
          revision = versionInfo.get('revision')

    # Get the revision from the object status, as a last resort (latest revision)
    revision = revision or objStat.get('revision')

    # Get the object info
    objInfo = self.nodes.pullObjectInfoFrom(node, ownerId, revision=revision)

    # Log the object we are pulling
    DiscoverManager.Log.write(f"Pulling {objInfo['type']} {objInfo['name']} {version or ''}{('@' + revision) if revision else ''}")

    # Clone remote object
    git = self.nodes.repositoryFrom(node, ownerId, revision = revision)

    # Get reference to temporary object
    obj = Object(path=git.path, id=ownerId, revision=revision, info=objInfo, identity=identity)

    # Store base object
    self.objects.write.store(obj, identity=identity)

    # Make this object visible to all
    self.permissions.update(id = ownerId, canRead=True, canWrite=False, canClone=True)

    # For each known version, verify and store the tag
    if isinstance(versions, list):
      for versionInfo in versions:
        # Verify the signature against the identity
        # Optionally: only allow identities with a relationship with the object identity
        if versionInfo.get('identity') == identity and "signature" in versionInfo:
          signatureInfo = versionInfo["signature"]
          published = Datetime.from8601(versionInfo.get('published'))
          signature = b''
          if signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64" and signatureInfo.get('digest') == "SHA512":
            signature = base64.b64decode(signatureInfo.get('data', '').encode('utf-8'))

          obj = self.objects.retrieve(id = ownerId, revision = versionInfo.get('revision', revision))
          if obj is None:
            # We need to discover this version in order to verify it, so we pass it up
            continue

          verifyKeyId = signatureInfo.get('key', '')
          versionTag = versionInfo["version"]
          if self.keys.verifyTag(obj, signature, identity, verifyKeyId, versionTag, published):
            # Store version tag
            self.versions.write.update(obj, versionTag, identity, published, signature, verifyKeyId)
            DiscoverManager.Log.noisy(f"Discovered version {versionInfo['version']}")
          else:
            DiscoverManager.Log.noisy("Version rejected due to signature mismatch.")

    # Get a reference to this object
    obj = self.objects.retrieve(id = ownerId, revision = revision, version = version)
    # TODO: Handle when the version requested isn't actually found
    if obj is None:
      return None

    revision = obj.revision

    # Now pull a build (if requested)
    if withBuild:
      # Get a build ID
      builds = self.nodes.pullBuildsFrom(node, ownerId, revision)
      builds = builds.get('builds', [])
      if len(builds) > 0:
        build = builds[0]
        buildId = build['id']

        DiscoverManager.Log.write(f"Pulling build for {objInfo['type']} {objInfo['name']} {obj.version or ''}")

        # Get a copy of the build task
        self.discover(uuid = build.get('id'), revision = build.get('revision'), person = person, withBuild = False)
        task = self.objects.retrieve(id = build.get('id'), revision = build.get('revision'))

        # Get the actual build into a temporary space
        import tempfile
        buildPath = os.path.realpath(tempfile.mkdtemp(prefix="occam-", dir=Config.tmpPath()))
        self.nodes.buildFrom(node, ownerId, revision, buildId, identity, buildPath)

        # Verify it
        # Get the hash of the build
        signatureInfo = build['signature']
        published = Datetime.from8601(build.get('published'))
        signed = Datetime.from8601(signatureInfo.get('signed'))
        verifyKeyId = signatureInfo.get('key', '')
        signature = b''
        if signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64" and signatureInfo.get('digest') == "SHA512":
          signature = base64.b64decode(signatureInfo.get('data', '').encode('utf-8'))

        if self.builds.verify(obj, task, identity, verifyKeyId, signature, published, signed, buildPath = buildPath):
          DiscoverManager.Log.noisy(f"Discovered build {buildId}.")
        else:
          DiscoverManager.Log.noisy("Build rejected due to signature mismatch.")

        # Pull build log
        buildLogDir = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
        buildLogPath = self.nodes.buildLogFrom(node, ownerId, revision, buildId, buildLogDir)

        # Store it
        self.builds.write.pull(obj, build, buildPath, buildLogPath)

        # Delete the temporary path
        import shutil
        shutil.rmtree(buildPath)
        shutil.rmtree(buildLogDir)

        # Pull Run-time Dependencies from build
        taskInfo = self.objects.infoFor(task)
        buildTaskInfo = taskInfo.get('builds', {})
        dependencies = buildTaskInfo.get('init', {}).get('dependencies', []) + buildTaskInfo.get('dependencies', [])
        for dependency in dependencies:
          if dependency.get('inject') in ["run", "init"]:
            localObject = self.objects.retrieve(id = dependency.get('id'), revision = dependency.get('revision'), version = dependency.get('version'))
            if not localObject:
              self.discover(uuid = dependency.get('id'), revision = dependency.get('revision'), version = dependency.get('version'), person = person)

    buildResources = []
    if withBuildDependencies:
      buildResources = objInfo.get("build", {}).get("install", [])

    # Pull resources
    if withResources:
      for resourceList in [objInfo.get("install", []), buildResources, objInfo.get("run", {}).get("install", [])]:
        for resource in resourceList:
          localObject = self.objects.retrieve(id = resource.get('id'), revision = resource.get('revision'), version = resource.get('version'))
          if not localObject:
            self.discover(uuid = resource.get('id'), revision = resource.get('revision'), version = resource.get('version'), person = person)

    buildDependencies = []
    if withBuildDependencies:
      buildDependencies = objInfo.get("build", {}).get("dependencies", [])

    # Pull dependencies
    if withDependencies:
      for dependencyList in [objInfo.get("dependencies", []), buildDependencies, objInfo.get("run", {}).get("dependencies", [])]:
        for dependency in dependencyList:
          localObject = self.objects.retrieve(id = dependency.get('id'), revision = dependency.get('revision'), version = dependency.get('version'))
          if not localObject:
            self.discover(uuid = dependency.get('id'), revision = dependency.get('revision'), version = dependency.get('version'), person = person)

    # Return an instantiation of this Object (the original, not the owner)
    return obj

  def discover(self, uuid, revision=None, version=None, withBuild = True, withBuildDependencies = False, person = None, nodes = None):
    """ Discovers an object on the network.

    Uses the various storage backends to hopefully find and retrieve the object
    information. Returns the Object that has been discovered.
    """

    DiscoverManager.Log.write(f"Attempting to discover {uuid} {version or ''}{('@' + revision) if revision else ''}")

    hosts = None

    if nodes:
      hosts = list(map(lambda x: x.host, nodes))

    hosts = hosts or self.storage.discover(uuid, revision=revision)

    # If we don't find nodes in the federation, we can elect to search nodes
    # directly...
    # TODO: This is likely... not a good plan!
    if len(hosts) == 0:
      nodes = self.nodes.findObject(uuid, revision = revision)

      if len(nodes) > 0:
        hosts = list(map(lambda x: x.host, nodes))

    if len(hosts) > 0:
      # Start asking the hosts for object metadata
      for host in hosts:
        hostURL = "https://%s:9292" % (host)

        # TODO: Optionally, we can also start discovering the nodes via:
        #   self.nodes.discover(hostURL)  # ... for host in hosts

        # TODO: We can maybe select from the list any nodes we already know
        #   or trust

        node = self.nodes.search(hostURL)
        if node is None:
          node = self.nodes.discover(hostURL, untrusted=True, quiet=True)

        if not node is None:
          # Discover the identity of this object owner
          objStat = self.nodes.statusFrom(node, SimpleNamespace(id = uuid, revision = revision, path = None), person = person)
          uri = objStat.get("identity")
          try:
            self.keys.identityFor(uri)
          except:
            self.retrieveIdentity(uri, person = person, nodes = [node])

          if objStat.get('type') == "resource":
            obj = self.pullResource(node, objStat, uuid, revision = revision, person = person)
          else:
            obj = self.pullObject(node, objStat, uuid,
                                  revision              = revision,
                                  version               = version,
                                  withBuild             = withBuild,
                                  withBuildDependencies = withBuildDependencies,
                                  person                = person)

          return obj

def network(name):
  """ This decorator will register the given class as a discovery backend.
  """

  def register_network(cls):
    DiscoverManager.register(name, cls)
    cls = loggable("DiscoverManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_network
