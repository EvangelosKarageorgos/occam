# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager

from occam.manager import uses

@command('builds', 'new',
  category      = 'Build Management',
  documentation = "Starts a build of the given object.")
@argument("object", type="object", nargs="?")
@option("-n", "--run",       action  = "store_true",
                             dest    = "on_run",
                             help    = "when specified, it builds all runtime dependencies.")
@option("-r", "--recursive", action  = "store_true",
                             dest    = "recursive",
                             help    = "when specified, builds all necessary objects when needed.")
@uses(ObjectManager)
@uses(BuildWriteManager)
@uses(ManifestManager)
@uses(JobManager)
class BuildsNewCommand:
  def build(self, obj, penalties, local):
    info = self.objects.infoFor(obj)

    Log.write("Building %s %s[@%s]" % (info.get('name'), obj.version + " " if obj.version else "", obj.revision))
    task = None
    resolved = False
    while(not resolved):
      try:
        task = self.manifests.build(obj, id = obj.id, revision = obj.revision, local = local, penalties = penalties, person = self.person)
        resolved = True
      except BuildRequiredError as e:
        if self.options.recursive:
          ret = self.build(e.requiredObject, penalties, local = False)
          if ret != 0:
            return ret
        else:
          Log.error("A dependency also needs to be built first. Use the --recursive flag to build all.")
          resolved = True
          task = None

    if task is None:
      Log.error("Could not generate a build task.")
      return -2

    tasks = [task]

    for task in reversed(tasks):
      #if self.options.task_only:
      if False:
        import json

        ret = {
          "id":       task.id,
          "uid":      task.uid,
          "revision": task.revision
        }

        Log.output(json.dumps(ret))
        return 0

      taskInfo = self.objects.infoFor(task)
      originalTaskId = task.id
      taskInfo['id'] = task.id
      opts = self.jobs.deploy(taskInfo, revision = task.revision, local=local, person = self.person, interactive=False)
      report = self.jobs.execute(*opts)

      elapsed = report['time']
      buildPath = report.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')
      buildLogPath = os.path.join(os.path.dirname(report.get('paths').get('task')), 'task', 'objects', str(taskInfo.get('builds').get('index')), 'stdout.0')

      if not local:
        self.builds.write.store(self.person.identity, obj, task, buildPath, elapsed, buildLogPath)

    return 0

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to build.")
      return -1

    local = False
    if self.options.object is None or self.options.object.id == '.':
      local = True

    obj = self.objects.resolve(self.options.object,
                               person = self.person)

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    # Get a task
    penalties = {}

    ret = -1

    if self.options.on_run:
      Log.header("Starting build of runtime requirements")

      info = self.objects.infoFor(obj)

      objInfos = info.get('run', {}).get('dependencies', []) + info.get('dependencies', [])
      objs = [self.objects.retrieve(id = info.get('id'), version = info.get('version'), revision = info.get('revision'), person = self.person) for info in objInfos]

      for subobj in objs:
        subInfo = self.objects.infoFor(subobj)
        if not self.builds.retrieveAll(subobj):
          if 'build' in subInfo:
            ret = self.build(subobj, penalties, local=False)
            if ret != 0 and ret != -2:
              return ret
          Log.write("Build not required for %s %s[@%s]: No build information" % (subInfo.get('name'), subobj.version + " " if subobj.version else "", subobj.revision))
        else:
          Log.write("Build already done for %s %s[@%s]" % (subInfo.get('name'), subobj.version + " " if subobj.version else "", subobj.revision))
    else:
      Log.header("Starting build")
      ret = self.build(obj, penalties, local)

    return ret
