# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore
from occam.databases.manager import DataNotUniqueError

from occam.services.database import ServiceDatabase

@datastore("services.write", reader=ServiceDatabase)
class ServiceWriteDatabase:
  """ Manages the database interactions for the private aspects of the Key component.
  """

  def updateServices(self, id, revision, environment, architecture, services):
    if len(services) == 0:
      return

    from occam.services.records.service import ServiceRecord

    # Create transaction
    session = self.database.session()

    for service in services:
      record = ServiceRecord()
      record.internal_object_id = id
      record.revision           = revision
      record.environment        = environment
      record.architecture       = architecture
      record.service            = service

      try:
        self.database.update(session, record)
      except DataNotUniqueError:
        pass

    self.database.commit(session)
