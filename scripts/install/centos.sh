# Package Dependencies
yum install -y wget gcc make zlib-devel openssl-devel ncurses-devel bzip2-devel readline-devel curl libcurl-devel xz-devel

mkdir -p ./vendor

export LD_LIBRARY_PATH=$PWD/vendor/lib:$LD_LIBRARY_PATH
export PATH=$PWD/vendor/bin:$PATH

# Build SQLite3
if [ ! -d sqlite-snapshot-201905242258 ]; then
	wget https://www.sqlite.org/snapshot/sqlite-snapshot-201905242258.tar.gz
	tar xvf sqlite-snapshot-201905242258.tar.gz
	cd sqlite-snapshot-201905242258
	./configure --prefix=$PWD/../vendor
	make -j4
	make install
	cd ..
fi

# Build Python

if [ ! -d Python-3.6.3 ]; then
	wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz
	tar xvf Python-3.6.3.tgz
	cd Python-3.6.3
	./configure --with-ensurepip=install --prefix=$PWD/../vendor --enable-loadable-sqlite-extensions LDFLAGS="-L$PWD/../vendor/lib" CPPFLAGS="-I$PWD/../vendor/include"
	make -j4
	make install
	cd ..
fi

# Setup from here
./scripts/install/common.sh
