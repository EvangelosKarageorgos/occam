#!/bin/bash

# Install required packages
sudo pacman -Sy python python-pip gcc libffi sqlite3 openssl --noconfirm -q

# Setup from here
./scripts/install/common.sh
