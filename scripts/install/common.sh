#!/bin/bash

# Install python environment
echo "Installing Python Virtual Environment"
python3 -m venv python

# Instantiate that python environment
echo "Instantiating Python Environment"
source ./python/bin/activate

# Install python dependencies
echo "Installing Python Dependencies"
# Necessary for some Docker environments
pip3 install wheel
# Install the actual dependencies
pip3 install -r dev-requirements.txt

# Build documentation
echo "Building Documentation"
cd docs
make html
cd ..

# Run Occam
echo "Initializing Occam"
./bin/occam system initialize
